package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.PreparedStatement;

import model.Movie;
import persistence.MovieDAO;

/**
 * Servlet implementation class MovieController
 */
@WebServlet(urlPatterns = { "/movies", "/movies/*" })
public class MovieController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MovieDAO dao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] pieces = getUriPieces(request);
     System.out.println("doGet");
        int length = pieces.length;
        if (length == 3) {
            // "/index"
            System.out.println("longitud 3");
            index(request, response);
        } else if (length > 3) {
            String piece = pieces[3];
            if (piece.equals("create")) {
                // new
                create(request, response);
            } else if (piece.equals("index")){
                index(request, response);
            } else {
                int id = 0;
                try {
                    id = Integer.parseInt(piece);
                } catch (Exception e) {
                    response.sendRedirect(request.getContextPath());
                }
                // show
                if (length == 4) {
                    //show(request, response, id);
                } else {
                    piece = pieces[4];
                    if (piece.equals("edit")) {
                 //       edit(request, response, id);                        
                    } else if (piece.equals("delete")){
                    	dao.delete(id);                        
                    } else {
                        response.sendRedirect(".");
                    }
                }}}

        	response.sendRedirect(request.getContextPath() + "/movies/index");
        
        }
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 System.out.println("en doPost");
	        String[] pieces = getUriPieces(request);

	        int length = pieces.length;
	        if (length == 3) {
	            System.out.println("longitud 3 store");
	            Movie movie = new Movie();
	            
	            movie.setTitle(request.getParameter("title"));
	            movie.setDirector(request.getParameter("director"));
	            try {
	                movie.setYear(Integer.parseInt(request.getParameter("year")));
	            } catch (Exception e) {
	            	movie.setYear(0);
	            }
	            System.out.println("insert");
	            
	            dao.insert(movie);
	            
	            
	            response.sendRedirect(request.getContextPath() + "/movies/index");
	        }
	}
	
	
	 private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/movies/index.jsp");
	        
	        ArrayList<Movie> movies = null;
	        try {
	            MovieDAO dao = new MovieDAO();
	            movies = dao.all();
	        } catch (SQLException ex) {
	            // TODO: handle exception
	            System.out.println("Fallo en movies.all()");
	            System.out.println("SQLException: " + ex.getMessage());
	        }
	        
	        request.setAttribute("movies", movies);
	        dispatcher.forward(request, response);
	    }
	 
	 private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/movies/create.jsp");
        dispatcher.forward(request, response);
    }
	 

    private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
        System.out.println(uri);

        String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            System.out.println(i + ": " + piece);
            i++;
        }
        return pieces;
    }
}
